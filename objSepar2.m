clear all
close all
clc


% sIm = imread('segmentedKariotype.bmp');
% bw = imread('segmentationMask.bmp');

imm = im2double(imread ('segmented\chromosome5.bmp'));
d = 8;
im = zeros(size(imm)+d);
im(d/2+1:end-d/2,d/2+1:end-d/2)=imm;
figure;imshow(im,[]);
sz = size(im);

%% segmentace 2

[~,prah] = edge (im,'Canny');
slabeH = prah(1)*3;
silneH = prah(2)*2.5;
hrany = edge (im,'Canny',[slabeH,silneH]);

figure;imshow(hrany,[]);
contur = zeros(size(hrany));
contur(hrany == 1) = 255;

figure;imshow(contur,[]);

bw = logical(contur);
bwFill = imfill(bw, 'holes');
bwClose = imclose (bwFill,strel('diamond',3));
bw = bwClose;

figure;imshow(bwClose,[]);

%% jasovy profil

% bw = im2bw(im,graythresh(im));
skelet = bwmorph(bw,'skel',Inf);
skeletThin = bwmorph(bw,'thin',Inf);
endPoints = bwmorph(skeletThin,'endpoints');
crossingPoints = bwmorph(skeletThin,'branchpoints');

figure;imshow(skelet,[]);
figure;imshow(endPoints,[]);
figure; imshow(skeletThin,[]);
figure; imshow(crossingPoints,[]);
% [contur,I] = contour_trace(skelet,endPoints(1));

% brightProfile = zeros(sz);
% [brightProfile(skelet == 1) = im;
%% 
[x_cross,y_cross]=find(crossingPoints==1);
[x_end,y_end]=find(endPoints==1);

numCross=length(x_cross);
numEnd=length(x_end);
m=zeros([numCross numEnd]);
q=zeros([numCross numEnd]);
for i=1:numCross
    for j=1:numEnd
    dx=abs(x_cross(i)-x_end(j)); %distance between end point and crossing point on verical axis
    dy=abs(y_cross(i)-y_end(j)); %distance between end point and crossing point on horizontal axis
    m(i,j)=tan(dx/dy); % direction of the line
    q(i,j)=y_end(j)-m(i,j)*x_end(j); %point where line cross vertical axis
    end
end

  error=0.2; %error tolerance
  for l=1:numCross
  for o=1:numEnd-1 
   for k=1:numEnd
       v=m(l,o);
       u=m(l,k);
          if v~=u % if the direction isn't same
              if(abs(v+u)<=error) % if difference between two directions is lower or equal to error (each direction has to have different mark)
              disp(['Shoda sm�rnic  ',num2str(v),'  a  ' ,num2str(u)]);
              m(l,o)=0; 
              m(l,k)=0;
              end
          end
   end
  end
  end

