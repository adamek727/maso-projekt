clear all;
close all;
clc;

chromosomes = uint8([]);
skeletons = logical([]);
noOfChromosomes = 46;

% read all segmented images
for i = 1 : noOfChromosomes

    path = sprintf('segmented\\chromosome%d.bmp',i);
    chrm = imread(path);
    path = sprintf('segmented\\skelet%d.bmp',i);
    skel = imread(path);
    maxX = max(size(chrm,2), size(chromosomes,2));
    maxY = max(size(chrm,1), size(chromosomes,1));
    
    for j = 1 : i
        chromosomes(maxY, maxX, j) = 0;
    end
    chrm(maxY, maxX) = 0;
    skel(maxY, maxX) = 0;
    
    chromosomes(:,:,i) = chrm;
    skeletons(:,:,i) = skel;
end

% plot all segmented images
figure
for i = 1 : size(chromosomes,3)
    subplot(5,10,i)
    imshow(chromosomes(:,:,i))
end




% Possible to select two different methods to pair chromosomes !
%[pairedIndexes, pairtedImages] = pairChromosomesByArea(chromosomes);
%[pairedIndexes, pairtedImages] = pairChromosomesByHistogram(chromosomes);
[pairedIndexes, pairtedImages] = pairChromosomesByAxisCorrelation(chromosomes, skeletons);



figure
for i = 1 : size(pairtedImages,3)
    subplot(5,5,i);
    imshow(pairtedImages(:,:,i))
end

%create new image with all paired chromosomes
spacing = 50;
pairedImagesSize = size(pairtedImages(:,:,1)) + spacing;
pairtedImages(pairedImagesSize(1),pairedImagesSize(2),:) = 0;


finalImage = uint8(zeros([pairedImagesSize(1)*4, pairedImagesSize(2)*6]));
for i = 0 : size(pairtedImages,3)-1
    finalImage( (mod(i,4)*pairedImagesSize(1)) +1+50 : (mod(i,4)*pairedImagesSize(1)) + pairedImagesSize(1)+50 , (floor(i/4)*pairedImagesSize(2)) + 1+50 : (floor(i/4)*pairedImagesSize(2)) + pairedImagesSize(2)+50  ) = pairtedImages(:,:, mod(i,4)*6+floor(i/4)+1 );
end
figure
imshow(finalImage);
imwrite(finalImage,'segmented/finalImage.bmp');

