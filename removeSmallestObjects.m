function [ reducedSegmentedImage ] = removeSmallestObjects( segmentationMask, noOfLeftObjects )

    cc = bwconncomp(segmentationMask);
    noOfObjects = cc.NumObjects;
    pixelIndexes = cc.PixelIdxList;

    % get all objects areas
    objectsAreas = zeros(1,noOfObjects);
    for i = 1 : noOfObjects
        objectsAreas(i) = length(pixelIndexes{i});
    end

    % sort all objects by area descending
    sortedObjectAreadIndexes = zeros(1,noOfObjects);
    for i = 1 : noOfObjects

        maxAreaIndex = find(objectsAreas == max(objectsAreas));
        sortedObjectAreadIndexes(i) = maxAreaIndex(1);
        objectsAreas(maxAreaIndex(1)) = 0;
    end

    % color objects based on their size
    reducedSegmentedImage = logical(zeros(size(segmentationMask)));
    for i = 1 : noOfObjects
        if i <= noOfLeftObjects
            reducedSegmentedImage(cc.PixelIdxList{sortedObjectAreadIndexes(i)}) = 1;
        end
    end

end

