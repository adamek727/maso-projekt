function [  pairedIndexes, pairtedImages ] = pairChromosomesByHistogram( chromosomes )
%PAIRCHROMOSOMESBYHISTOGRAM Summary of this function goes here
%   Detailed explanation goes here


    %% Histogram based pairing
    
    % create all cumulative histograms
    cumulativeHistograms = [];
    for i = 1 : size(chromosomes,3)
        h = imhist(chromosomes(:,:,i));
        h(1) = 0;
        cumulativeHistograms(i,:) = cumsum(h); 
    end

    % create comparation matrix
    H = zeros(size(chromosomes,3),size(chromosomes,3));
    for i = 1 : size(chromosomes,3)
        for j = 1 : size(chromosomes,3)
            H(i,j) = max(abs(cumulativeHistograms(i,:) - cumulativeHistograms(j,:)));
        end
    end
    H(H==0) = Inf;
    % pair images by H matrix
    pairedIndexes = zeros(size(chromosomes,3)/2,2);

    iterator = 1;
    for i = 1 : size(chromosomes,3)
        if min(H(i,:)) == Inf
            continue
        end

        pairedIndexes( iterator, 1) = i;
        nearestIndex = find( H(i,:) == min(H(i,:)));
        pairedIndexes( iterator, 2) = nearestIndex;
        
        H(i,:) = Inf;
        H(:,i) = Inf;
        H(nearestIndex,:) = Inf;
        H(:,nearestIndex) = Inf;
        iterator = iterator+1;
    end
    
    
    %create new paired images 
    maxX = size(chromosomes,2);
    maxY = size(chromosomes,1);
    pairtedImages = uint8(zeros(maxY, 2*maxX, size(chromosomes,3)/2));
    for i = 1 : size(chromosomes,3)/2
        index1 = pairedIndexes(i,1);
        index2 = pairedIndexes(i,2);
        pairtedImages(:,:,i) = [chromosomes(:,:,index1)  chromosomes(:,:,index2)];
    end
    
end

