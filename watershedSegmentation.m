inputImg=imread('kariotypes/ext1.png');
inputImg=rgb2gray(inputImg);
inputImg=im2double(inputImg);

normalizedHeight = 500;

inputImg = imresize(inputImg, normalizedHeight/size(inputImg,1));


 inputImg = medfilt2(inputImg, [5 5]);
 Sobelx = [-1 0 1;-2 0 2; -1 0 1]; 
 Sobely = Sobelx'; 
 Fx = conv2(inputImg,Sobelx,'same'); 
 Fy = conv2(inputImg,Sobely,'same'); 
 F = sqrt(Fx.*Fx+Fy.*Fy);
 
 FKvant = grayslice(F,7);
 watershedImage = watershed(FKvant);
 outputImg = logical(zeros(size(watershedImage)));
 
 outputImg(watershedImage ~= 1) = 1;
 
 imshow (outputImg,[])