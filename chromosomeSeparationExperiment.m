clear all;
close all;
clc;


xPlot = 3;
yPlot = 2;

img = imread('segmented\chromosome1.bmp');
subplot(yPlot, xPlot, 1)
imshow(img)

segmentationMask = logical(zeros(size(img)));
segmentationMask(img ~= 0) = 1;
subplot(yPlot, xPlot, 2)
imshow(segmentationMask)

distanceImage = bwdist(~segmentationMask);
subplot(yPlot, xPlot, 3)
imshow(distanceImage,[])

% non max supression
nonMaxSupressedImage = logical(zeros(size(distanceImage)));
for i = 2 : size(distanceImage,1)-1
    for j = 2 : size(distanceImage,2)-1
        subMatrix = distanceImage(i-1:i+1, j-1:j+1);
        if max(max(subMatrix)) == subMatrix(2,2);
            nonMaxSupressedImage(i,j) = 1;
        end
    end
end

subplot(yPlot, xPlot, 4)
imshow(nonMaxSupressedImage,[])

invertedDistanceImage = -distanceImage;
invertedDistanceImage(invertedDistanceImage == 0) = -Inf;

subplot(yPlot, xPlot, 5)
imshow(invertedDistanceImage,[]);


watershedImage = watershed(medfilt2(invertedDistanceImage,[5 5]));

subplot(yPlot, xPlot, 6)
imshow(watershedImage,[]);
