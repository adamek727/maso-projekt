function [bwmask,edge, outmask] = touchSepar(inmask)
% Funkce oddeluje dotykajici se chromosomy.
% vstupy: 
%   - 'inmask' binarni obraz karyotypu
% vystupy: 
%   - 'bwmask' binarni obraz kde jsou dotykajici chormozomy oddeleny
%   - 'edge' hrany objektu detekovane pomoci watersheed
%   - 'outmask' sedotonovy obraz jednotlive objekty jsou repr. jednou urvni
%   sedi

% Priklad spusteni:
%  [bwmask,edge,outmask] = touchSepar(imread('segmentationMask.bmp'));

%% telo funkce

% definovani vstupu
mask = inmask;
sz = size(mask);

% morfologicke upravy vstupu
%mask = imfill(mask,8,'holes');   %vyplneni malych der
mask = imopen (mask,strel('diamond',3)); %otevreni, podporeni separace objektu 

% distancni mapa
distMap = ~mask;
distMap = bwdist(distMap);

% uprava dist mapy pro segmentaci
distMap = distMap.^2;   %zvyrazneni maxim
trash = mean(mean(distMap(mask~=0)));   %prahovani hran
distMap(distMap>trash)=max(max(distMap));
distMapInverz = distMap;
distMapInverz(mask == 0) = Inf;
distMapInverz = - distMapInverz;
maskSegmented = watershed(distMapInverz); %watersheed
outmask = maskSegmented;

% hrany objektu
edge = (zeros(sz));
edge(maskSegmented == 0) = 255;
edge = logical(edge);

% prevedeni segmentace na binarni obraz
bwmask = zeros(sz);
bwmask(maskSegmented>1)=255;
bwmask = logical(bwmask);
bwmask = bwareaopen(bwmask,50); %odstraneni objektu mensich nez 50. Konstatnta 50 je odvozena z nasledujiciho skriptu
bwmask(mask == 0) = 0;

% skript pro manualni odvozeni velikosti objektu, ktere chceme odstranit.
% ktery seradi velikosti objektu vzestupne

% cc = bwconncomp(chromHolesFill);
% % serazeni velikosti objektu vzestupne
% velikostiObjektu = sort(cellfun('length',cc.PixelIdxList));
% velikosti = sort(velikostiObjektu);

%% kontrolni vizualizace
% 
% % 3D vizualizace distancni mapy
% figure(1)
% mesh(distMap)
% 
%vykresleni vystupu
figure(2)
subplot(2,2,1); imshow(inmask,[]); title('vstupni obraz');
subplot(2,2,2); imshow(maskSegmented,[]); title('vystup segmentace');
subplot(2,2,3); imshow(bwmask,[]); title('vystupni bw maska');
subplot(2,2,4); imshow(edge,[]); title('hrany objektu');
end




