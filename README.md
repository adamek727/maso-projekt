﻿# MASO Projekt

Hlavní program pro segmentaci, detekci, separaci, a párování chromozomů je spouší skriptem main.m
Ten postupně spouští skripty segmentationScript.m, objectSeparation.m, pairSegmentedChromosomes.m
Ty postupně segmentují chromozomy, provádí jejich vyříznutí do zvláštních obrázků a v poslední fázi provedou jejich zpárování.

Jednotlivé dílčí funkce, které jsou použity v běhu hlavního skriptu jsou vždy odděleny do zvláštního souboru

V separovaných funkcích je řešená problematika dotýkajících se chromozomů (touchSepar.m) a křížení chromozomů (crossElimination.m) 

