function [ outputImg ] = segmentByEdges( inputImg )
    
    % this function takes grayscale image and tending to separate objets
    % based on edged closed regions

    edgeImage = edge(inputImg,'canny');

    imageToFill = edgeImage;
    for i = 1 : 3
       se = strel('disk',3);
       imageToFill = imdilate(imageToFill,se);
       imageToFill = imfill(imageToFill,'holes');
       imageToFill = imerode(imageToFill,se);
    end

    se = strel('disk',3);
    outputImg = imopen(imageToFill, se);

end

