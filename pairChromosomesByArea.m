function [ pairedIndexes, pairtedImages ] = pairChromosomesByArea( chromosomes )

    % calculate chromosome areas
    chromosomeAreas = []
    for i = 1 : size(chromosomes,3)
        chrm = chromosomes(:,:,i);
        chromosomeAreas(i) = size(chrm(chrm ~= 0),1);
    end 
    chromosomeAreas(2,:) = 1 : size(chromosomes,3)


    % sort all areas and their indexes of chromosomes
    [Y I] = sort(chromosomeAreas(1,:),'descend');
    sortedChromosomeAreas = chromosomeAreas(:,I);

    % half size of all chromosomes number
    pairedChromosomesIndexes = zeros(size(chromosomes,3)/2,2);

    
    % create pairs
    for i = 1 : size(chromosomes,3)/2
        pairedChromosomesIndexes(i,:) = sortedChromosomeAreas(2,1+(i-1)*2 : 2+(i-1)*2);
    end

    %create new paired images 
    maxX = max(size(chrm,2), size(chromosomes,2));
    maxY = max(size(chrm,1), size(chromosomes,1));
    pairedChromosomes = uint8(zeros(maxY, 2*maxX, size(chromosomes,3)/2));
    for i = 1 : size(chromosomes,3)/2
        index1 = pairedChromosomesIndexes(i,1);
        index2 = pairedChromosomesIndexes(i,2);
        pairedChromosomes(:,:,i) = [chromosomes(:,:,index1)  chromosomes(:,:,index2)];
    end

    pairedIndexes = pairedChromosomesIndexes;
    pairtedImages = pairedChromosomes;

end

