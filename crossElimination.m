function [outMask,outSkelet,skeletLine,lineSkeletLength] = crossElimination (inMask)
% Funkce odstranuje krizici chromozomy pomoci urceni smernic jejich jednotlivich linii mezi konecovymi body
% a body krizeni
% vstupy: 
%   - 'inmask' segmentovany binarni obraz 1-cca 5 chrom, ktere chceme
%   separovat. Pri vice chromozomech by kriterium smernice nebylo
%   vypovidajici
% vystupy: 
%   - 'outMask' vystupni maska kde jednotlive chromozomy jsou urceny jasem
%   v sedotonove skale. 1 - pocet chromozomu
%   - 'outSkelet' vystupni separovany skelet obou chromozomu, kde skelety
%   jednotlivych chromozomu jsou urceny jasem v sedonotove skale-1-pocet
%   chromozomu
%   - 'skeletLine' matice delkaSkeletu X pocetchromozomu. Ve sloupcich jsou indexi jednotlivzch sepravanych skeletu 1 sloupec =
%   skelet jednoho chromozomu
%   - 'lineSkeletLength' vektor delka jednotlivych skeletu. pozice vektoru
%   odpovida sloupci v skeletLine

% Priklad spusteni:
% inMask = imread ('segmented/segmented/chromosome46.bmp');
% [outMask,outSkelet,skeletLine,lineSkeletLength] = crossElimination (inMask);
% 
% figure; imshow(outMask,[])
% figure; imshow(outSkelet,[])
%--------------------------------------------------------------------------

bw=inMask;
sz = size(bw);
skelet = bwmorph(bw,'thin',Inf);

endPoints = bwmorph(skelet,'endpoints');
crossingPoints = bwmorph(skelet,'branchpoints');

%pokud existuje crossingPt funkce pokracuje, pokud ne funkce se ukonci vystupni maska bude
%stejna jako vstupni
if max(max(crossingPoints))~=0

[positionEP(2,:),positionEP(1,:)] = find(endPoints == 1);
[positionCP(2,:),positionCP(1,:)] = find(crossingPoints == 1);

%% vycisteni skeletu, odstraneni ocasku mensich nez 15px

Dmax = 15;
Dmask = false(size(skelet));
for k = 1:length(positionEP)
    D = bwdistgeodesic(skelet,positionEP(1,k),positionEP(2,k));
    distanceToBranchPt = min(min(D(positionCP(2,:),positionCP(1,:))));
    if distanceToBranchPt<Dmax
        Dmask(D < distanceToBranchPt)=true;
    end
end

skelet = skelet - Dmask;
endPoints = bwmorph(skelet,'endpoints');
crossingPoints = bwmorph(skelet,'branchpoints');

% pokud po vycisteni skeletu neni zadny crossPt funkce se ukonci vystupni maska bude
% stejna jako vstupni 
if max(max(crossingPoints))~=0

[EP(2,:),EP(1,:)] = find(endPoints == 1);
[CP(2,:),CP(1,:)] = find(crossingPoints == 1);

%% separace jednotlivych linii + vypocet smernice pro kazdou linii

skelet = logical(skelet);           %vycisteny skelet urceny k indexaci
skeletNew = skelet;                 %skelet pro postupne odecitani linii
angle = 0;                          %promena pro smernice

L=1;    %indexovani vektoru bodu
iter = length(CP)+length(EP)-1;     %pocet opakovani je nastaven s rezervou podle poctu vyznacnych bodu
lines = zeros(length(skelet),iter); %promena pro ukladani indexu jednotlivych linii
CP_iter=[0 0];                      %promena pro novy crospoint v kazdem cyklu
terminate = 0;                      %promen pro ukonceni, pokud bude 1, cyklus se ukonci

% V kazdem cyklu se postupne cisti skelet od konecnych linii od endPt ke
% crossPt. Kazda linie se takto ulozi, vsechny jejich indexi (lines) a
% delka (linesLength)
for cros = 1:length(CP)             %cyklus probehne tolikrat kolik je crossPt,pocet opakovani s rezervou 

    if terminate ~=1                %pokud jsou vsechny linie rozdelene skoci na konec cyklu

        endPoints = bwmorph(skeletNew,'endpoints');             %nove endPt po separaci prvnich koncovych linii skeletu                    
        crossingPoints = bwmorph(skeletNew,'branchpoints');     %nove crossPt po separaci prvnich koncovych linii skeletu 
    
    % podminka kdyz neexistuje crossPt
    if max(max(crossingPoints)) == 0                            % pokud v novem skeletu nejsou crossPt urci jako nove crossPt nove endPt 
         EPnew = zeros(2,length(find(endPoints)==1));
         CPnew = EPnew;
        [EPnew(1,:),EPnew(2,:)] = find(endPoints == 1);
        [CPnew(1,:),CPnew(2,:)] = find(endPoints == 1);
    else   
        EPnew = zeros(2,length(find(endPoints)==1));            % pokud crosPt existuji, oznac je jako nove crosPt pro dalsi beh cyklu
        CPnew = zeros(2,length(find(crossingPoints)==1));
        [EPnew(1,:),EPnew(2,:)] = find(endPoints == 1);
        [CPnew(1,:),CPnew(2,:)] = find(crossingPoints == 1);
    end

% ve vnitrnim cyklu se hleda nejblizsi crosPt skeletu k soucasnemu endPt.
% Nasledne se ulozi x,y suradnice endPt (xEP,yEP) a crossPt( xCP,yCP), indexi linie a delka
% linie
    for k = 1:length(EPnew)
        
        % dalsi podminka ukonceni
        if terminate ~=1                        %pokud jsou vsechny linie rozdelene skoci na konec cyklu
            
            Dmask = false(size(skeletNew));     %Maska pro postupne odecitani linii od celkoveho skeletu    
            D = bwdistgeodesic(skeletNew,EPnew(2,k),EPnew(1,k));        %vypocet distancni matice od soucasneho endPt k ostanim pixelum skeletu
            distanceToBranchPt = (min(min(D(CPnew(1,:),CPnew(2,:)))));  %vzdalenost k nejblizsimu crossPt se urci jako kriterium pro oznaceni cele linie
        
            if distanceToBranchPt<5                 %pokud je crossPt priliz blizko, ignoruje se hleda se druhy nejbli
                pom = find(D==distanceToBranchPt);
                pom2=D(pom);
                D(pom) = max(max(D));
                D(D == distanceToBranchPt) = max(max(D));
                distanceToBranchPt = (min(min(D(CPnew(1,:),CPnew(2,:)))));
                D(pom) = pom2;
            end          %Cyklus tady selhava pokud jsou dva crospointy prilis blizko sebe, najde pouze prvni nejblizsi a druhy, ktery je taky blizko oznaci jako hledany crossPt. Nasledkem je prilis kratka linie, ktera zkresli vyslednou masku
 
 % ukoncovaci podminka, pokud ma nalezeny crosPt distanci rovnou
 % maximalni hodnote, probehne posledni indexace linie, urceni smernice, odecteni od
 % skeletu. vysledkem je skelet, ktery ma vsude nuly
 % promena ukonceni se nastavi na terminate = 1
        if distanceToBranchPt == max(max(D));
            CP_iter = [EPnew(1,end),EPnew(2,end)];
            EPnew = [EPnew(1,1),EPnew(2,1)];
            Dmask(D <= distanceToBranchPt)=true;
            pom = find(Dmask==true);
            lineLength(L) = length(pom);
            lines(1:lineLength(L),L) = find(Dmask ==true);
            
        if EPnew(1)> CP_iter(1)
            angle(L) = (atan2d((CP_iter(1)-EPnew(1)),(CP_iter(2)-EPnew(2)))) + 180;
        else
            angle(L) = (atan2d((CP_iter(1)-EPnew(1)),(CP_iter(2)-EPnew(2)))); %- 180;
        end
        
            yEP(:,L) = EPnew(1);
            yCP(:,L) = CP_iter(1);
            xEP(:,L) = EPnew(2);
            xCP(:,L) = CP_iter(2);
            
            skeletNew=skeletNew-Dmask;
            skeletNew=logical(skeletNew);       
            
            terminate = 1;
        end
% konec ukoncovaci podminky

% pokracovani funkce
 
    if terminate ~= 1       %pokud jsou vsechny linie rozdelene skoci na konec cyklu
 
        %osetreni pokud se najde vice odu se stejnou distance, vybere se prvni z nich ostatni se nastavi na hodnotu maximalni distance 
        while length(find(D == distanceToBranchPt))>1   
            pom = find(D==distanceToBranchPt);
            D(pom(2,:))=D(pom(1))+max(max(D));
        end

        Dmask(D < distanceToBranchPt)=true;                     % do distancni masky pro jeden cyklus (soucasny endPt) se ulozi cela linie od endPt ke crossPt
        [CP_iter(1),CP_iter(2)] = find (D==distanceToBranchPt); % jako novy crossPt se oznaci ten, ktery je vzdalenosti, ktera byla urcena v predesle fazi jako vzdalenost nejblizsho crossPt 
        pom = find(Dmask==1);                                   % do pom se ulozi indexi nalezene linie
        lineLength(L) = length(pom);                            % delka linie se ulozi do lineLength
        lines(1:lineLength(L),L) = find(Dmask ==true);          % kazdy sloupec promenne line obshuje indexi linie (casti skeletu) z puvodnim obrazu 
                
        if EPnew(1,k)> CP_iter(1)                               % vypocet smernice, pokud je y souradnice zacinajiciho bodu lline vetsi nez y souradnice koncoveho bodu linie, musi se pricist 180�
            angle(L) = (atan2d((CP_iter(1)-EPnew(1,k)),(CP_iter(2)-EPnew(2,k)))) + 180;
        else
            angle(L) = (atan2d((CP_iter(1)-EPnew(1,k)),(CP_iter(2)-EPnew(2,k)))); %- 180;
        end
        
        % body linii se neprolinaji, ty ktere byly v prvni behu jako
        % crossPt nejsou v dalsim behu jako endPt. Ale jako endPt se urci
        % bod hned vedle puvodniho crosPt
        yEP(:,L) = EPnew(1,k);          % vysledne y souradnice vsech endPt (pocatecni body linie)
        yCP(:,L) = CP_iter(1);          % vysledne y souradnice vsech crossPt (konecne body linie)
        xEP(:,L) = EPnew(2,k);          % vysledne x souradnice vsech endPt (pocatecni body linie)
        xCP(:,L) = CP_iter(2);          % vysledne x souradnice vsech crossPt (konecne body linie)
        
        L=L+1;                          % indexace neznameho poctu koncovych bodu linii
        
        
        skeletNew=skeletNew-Dmask;      % odecteni soucasne linie od zbytku skeletu
        skeletNew=logical(skeletNew);

    end;end % endy ukoncujicich podminek    
    end     % vnitrni cyklus
    end     % endy ukoncujicich podminek 
end         % vnejsi cyklus


%% vyhodnoceni shody smernic pomoci shlukove analyzy

numberOfEP=length(EP);                          %pravdepodobny pocet shluku se urci podle poctu endpointu

if mod(numberOfEP,2)==1
    ind=kmeans(angle',floor(numberOfEP/2)+1);   % shukova analyza, vstupem jako parametr jsou smernice a pocet vystupnich shluku
%     ind=kmeans([angle' xEP' yEP' xCP' yCP'],floor(numberOfEP/2)+1);
else
    ind=kmeans(angle',numberOfEP/2);
%     ind=kmeans([angle' xEP' yEP' xCP' yCP'],numberOfEP/2);
end

%% vytvoreni separovanych linii pro chromozomy

skeletLine = zeros(sum(lineLength),max(ind));       % indexi separovanych skeletu, radky podle souctu vsech delek lini (rezerva) a sloupce podle poctu chromozomu v obraze
outSkelet = uint8(zeros(sz));                       % vystupni skelet
outMask = uint8(zeros(sz));                         % vystupni maska
ch = zeros(max(ind),length(angle));                 % body jednotlivych chromozomu, pocet chromozomu odpovida poctu shluku, delka je s rezervou urcena podle poctu smernic

for i =1:max(ind)                                   % vnejsi cyklus probehne pro kazdy chromosom (pocet radku)
    chromSkelet = false(sz);
    ch(i,1:length(find(ind==i)))=find(ind==i);      % vyber pozic, ktere byly shlukovou analyzou urceny jako jeden chromozom, pozice hodnot pro kazdou smernici odpovida pozici vsech parametru vystupniho chromozomu v ostatnich promennych (lineLegth=delka, line=indexi linie skeletu)
    lineSkeletLength(i) = sum(lineLength(ch(i,1:length(find(ind==i))))); % vynalni skelet jednoho chromozomu vznikne jako spojeni nekolika mensich linii
    pom = zeros(lineSkeletLength(i),1);
    start = 1;                                      
    konec = 0;
    for j = 1:length(ch(i,1:length(find(ind==i))))
        konec = start+lineLength(ch(i,j))-1;                        % konecna pozice indexace je urcena podle delky soucasne linie v promenne lineLength
        pom(start:konec,1) = lines(1:lineLength(ch(i,j)),ch(i,j));  % postupne sestaveni skeletu jednoho chromozomu
        start=konec+1;
    end

    skeletLine(1:lineSkeletLength(i),i) = pom;
    chromSkelet(skeletLine(1:lineSkeletLength(i),i)) = 1;
    outSkelet = outSkelet + uint8(chromSkelet);
    outSkelet(chromSkelet==1) = i;
    
    chromMask = imdilate(chromSkelet, strel('disk',6));
    outMask = outMask + uint8(chromMask);
    outMask(chromMask==1) = i ;  %(2*i^2);
    
end
else
    outMask = inMask;
    skeletLine = find(skelet==1);
    lineSkeletLength = length(skeletLine);
    outSkelet =logical(skelet);
end
else
    outMask = inMask;
    skeletLine = find(skelet==1);
    lineSkeletLength = length(skeletLine);
    outSkelet = skelet;
    
end
%     figure(4)
%     imshow(outMask,[])
end