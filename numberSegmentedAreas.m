function [ numberedSegmentedImage ] = numberSegmentedAreas( segmentationMask)
    
    cc = bwconncomp(segmentationMask);
    noOfObjects = cc.NumObjects;
    pixelIndexes = cc.PixelIdxList;

    % get all objects areas
    objectsAreas = zeros(1,noOfObjects);
    for i = 1 : noOfObjects
        objectsAreas(i) = length(pixelIndexes{i});
    end

    % sort all objects by area descending
    sortedObjectAreadIndexes = zeros(1,noOfObjects);
    for i = 1 : noOfObjects

        maxAreaIndex = find(objectsAreas == max(objectsAreas));
        sortedObjectAreadIndexes(i) = maxAreaIndex(1);
        objectsAreas(maxAreaIndex(1)) = 0;
    end

    % color objects based on their size
    numberedSegmentedImage = uint8(zeros(size(segmentationMask)));
    for i = 1 : noOfObjects
        numberedSegmentedImage(cc.PixelIdxList{sortedObjectAreadIndexes(i)}) = i; % +20 for bettec contrast
    end
end

