clear all;
close all;
clc

img = imread('segmented\chromosome1.bmp');
mask = logical(zeros(size(img)));
mask(img ~= 0) = 1;

subplot(1,2,1);
imshow(mask,[]);

mask = imopen(mask, strel('disk',5));


subplot(1,2,2);
imshow(mask,[]);