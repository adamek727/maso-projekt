clear all
close all
clc


subwindowSizeHalf = 5;
subwindowSize = subwindowSizeHalf * 2 + 1;
angleStep = 45;

img = (imread('segmented\chromosome1.bmp'));

spectrumSpace = zeros(size(img,1) - subwindowSizeHalf*2-1 , size(img,2) - subwindowSizeHalf*2-1, floor(359/angleStep));

for d = 0 : angleStep : 359
    
    d/angleStep + 1
    
    mask = load('MaskaFrek.mat');
    mask = imrotate(mask.MaskaFrek,d);
    mask = imresize( mask, [subwindowSize,subwindowSize], 'bicubic' );
    
    
    imshow(mask);
    
    
    for i = 1 + subwindowSizeHalf : size(img,1)-subwindowSizeHalf
        for j = 1 + subwindowSizeHalf : size(img,2)-subwindowSizeHalf

            subwindow = img(i-subwindowSizeHalf : i + subwindowSizeHalf, j-subwindowSizeHalf : j + subwindowSizeHalf);
            subwindowSpectrum =  fftshift(log10(fft2(subwindow)));
            if ~isinf(sum(sum(subwindowSpectrum))) 
                %imshow(subwindowSpectrum,[]);
                subwindowSpectrum = subwindowSpectrum .* mask;
                meanVal = mean(mean(subwindowSpectrum));
                spectrumSpace(i-subwindowSizeHalf,j-subwindowSizeHalf,d/angleStep+1) = meanVal;
            else
                spectrumSpace(i-subwindowSizeHalf,j-subwindowSizeHalf,d/angleStep+1) = 0;
            end
        end
    end

end


for i = 1 : 7
    subplot(2,4,i)
    imshow(spectrumSpace(:,:,i),[]);
end
subplot(2,4,8)
imshow(img)

