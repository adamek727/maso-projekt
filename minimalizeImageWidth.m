function [ outputImg ] = minimalizeImageWidth( inputImg )

    %this function rotate image clockwised step by step by 5deg, to
    %possition, when it has smallest width

    minWidthRot = 0;
    minWidth = intmax;
    
    for i = 0 : 10 : 180
        
        rotatedImage = imrotate(inputImg, -i, 'loose', 'bilinear');
        %imshow(rotatedImage);
        [Y X] = find(rotatedImage ~= 0);
        minX = min(X);
        minY = min(Y);
        y = Y-minY+1; 
        x = X-minX+1;
        minX = min(x);
        minY = min(y);
        maxX = max(x);
        maxY = max(y);
        
        if (maxX-minX) < minWidth
            minWidth = maxX-minX;
            minWidthRot = -i;
        end
    end
    
    outputImg = imrotate(inputImg, minWidthRot, 'loose', 'bilinear');

end

