clear all;
close all;
clc;

noOfChromosomes = 46;

segmentedImage = imread('segmentedKariotype.bmp');
segmentationMask = imread('segmentationMask.bmp');

figure;
subplot(1,2,1); imshow(segmentedImage); title('segmentedImage')
subplot(1,2,2); imshow(segmentationMask); title('segmentation mask')

cc = bwconncomp(segmentationMask);
noOfObjects = cc.NumObjects;
pixelIndexes = cc.PixelIdxList;

if noOfObjects > noOfChromosomes
    display('Too much segmented objects. Remove smallest')
    segmentationMask = removeSmallestObjects(segmentationMask,noOfChromosomes);
elseif noOfObjects < noOfChromosomes
    display('Too few segmented objects. Some segmented objects contains more chromosomes')
    % add function for separating more object from current image
end

cc = bwconncomp(segmentationMask);
noOfObjects = cc.NumObjects;
pixelIndexes = cc.PixelIdxList;



if noOfObjects <= noOfChromosomes
    numberedSegmentedImage = numberSegmentedAreas(segmentationMask);
    
    figure
    imshow(numberedSegmentedImage*(255/noOfChromosomes)); % *4 for better contrast
    
    
    for i = 1 : noOfObjects
        
        chromosome = cropOutNonZeroContent(numberedSegmentedImage, segmentedImage, i);
        chromosome = minimalizeImageWidth(chromosome);
        chromosome = cropOutNonZeroContent(chromosome, chromosome, -1);
        
        %figure(10)
        %subplot(5,10,i);
        %imshow(chromosome,[]); title(sprintf('%d',i));
       
        %figure(11)
        %subplot(5,10,i);
        openedChromosome = imopen(chromosome,  strel('disk',7));
        skeleton = bwmorph(logical(openedChromosome),'thin',Inf); %bwmorph(logical(chromosome),'skel',Inf);title(sprintf('%d',i));
        %imshow(skeleton,[]);
        
        %figure(12);
        fusedImage = uint8(zeros([size(chromosome) 3]));
        fusedImage(:,:,1) = chromosome + uint8(skeleton)*255;
        fusedImage(:,:,2) = chromosome - uint8(skeleton)*255;
        fusedImage(:,:,3) = chromosome - uint8(skeleton)*255;
        %subplot(5,10,i);
        %imshow(fusedImage);
        
        fileName = sprintf('segmented/chromosome%d.bmp',i)
        imwrite(chromosome,fileName);
        fileName = sprintf('segmented/skelet%d.bmp',i)
        imwrite(skeleton,fileName);
        fileName = sprintf('segmented/fused%d.bmp',i)
        imwrite(fusedImage,fileName);
    end
end





% splash image to noOfChromosomes separated images for each chromosome