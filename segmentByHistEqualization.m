function [ outputImg ] = segmentedByHistEqualization( inputImg )

    equalisedImg = histeq(inputImg);
    
    th = 50/255;
    
    outputImg = ~im2bw( equalisedImg, th);
    
    se = strel('disk',3);
    outputImg = imopen(outputImg, se);
end

