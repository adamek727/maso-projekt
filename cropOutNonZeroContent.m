function [ outputImage ] = cropOutNonZeroContent( numberedImade, segmentedImage, i )
    % this function takes input image and crop out segmented image which is
    % specified by index i, if i==0, all non zero objects are cropped out
    
    if i == -1
        [Y X] = find(numberedImade ~= 0);
    else
        [Y X] = find(numberedImade == i);
    end
    
    minX = min(X);
    minY = min(Y);
    y = Y-minY+1; 
    x = X-minX+1;
    minX = min(x);
    minY = min(y);
    maxX = max(x);
    maxY = max(y);

    outputImage = uint8(zeros(maxY,maxX));
    for j = 1 : length(y)
        outputImage(y(j),x(j)) = segmentedImage(Y(j),X(j));
    end
    
    
end

