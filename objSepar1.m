clear all
close all
clc


% sIm = imread('segmentedKariotype.bmp');
% bw = imread('segmentationMask.bmp');

imm = im2double(imread ('segmented/chromosome5.bmp'));
d = 8;
im = zeros(size(imm)+d);
im(d/2+1:end-d/2,d/2+1:end-d/2)=imm;
figure;imshow(im,[]);
sz = size(im);

%% segmentace 2

[~,prah] = edge (im,'Canny');
slabeH = prah(1)*3;
silneH = prah(2)*2.5;
hrany = edge (im,'Canny',[slabeH,silneH]);

figure;imshow(hrany,[]);
contur = zeros(size(hrany));
contur(hrany == 1) = 255;

figure;imshow(contur,[]);

bw = logical(contur);
bwFill = imfill(bw, 'holes');
bwClose = imclose (bwFill,strel('diamond',3));
bw = bwClose;

figure;imshow(bwClose,[]);

%% jasovy profil

% bw = im2bw(im,graythresh(im));
skelet = bwmorph(bw,'skel',Inf);
skeletThin = bwmorph(bw,'thin',Inf);
endPoints = bwmorph(skelet,'endpoints');

figure;imshow(skelet,[]);
figure;imshow(endPoints,[]);
figure; imshow(skeletThin,[]);

% [contur,I] = contour_trace(skelet,endPoints(1));

% brightProfile = zeros(sz);
% [brightProfile(skelet == 1) = im;





