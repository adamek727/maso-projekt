function [ outputImage ] = segmentByStdDiv( inputImage )
    
    inputImageSize = size(inputImage);
    submatrixSize = 5;
    n = (submatrixSize-1)/2;
    stdDivImg = zeros(inputImageSize - n);


    for i = 1+n :  inputImageSize(1)-n-1
       for j = 1+n : inputImageSize(2)-n-1
          submatrix = inputImage(i-n:i+n+1, j-n:j+n+1);
          stdDivImg(i,j) = std2(submatrix);
       end
    end

    stdDivImgMax = max(max(stdDivImg));
    stdDivThIndexes = find(stdDivImg > stdDivImgMax/5);
    
    stdDivImg = zeros(size(stdDivImg));
    stdDivImg(stdDivThIndexes) = 100;
    

%     se = strel('disk',1);
%     imageToFill = imdilate(imageToFill,se);
%     imageToFill = imfill(imageToFill,'holes');
%     imageToFill = imerode(imageToFill,se);

    outputImage = zeros(size(inputImage));
    outputImage(1+n-1:end-n+1, 1+n-1:end-n+1) = im2bw(imfill(stdDivImg, 'holes'),0.5);
    
    se = strel('disk',3);
    outputImage = imopen(outputImage, se);
    
end

