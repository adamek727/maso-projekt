clear all;
close all;
clc;

display('loading image')
kariotypeImg = rgb2gray(imread('./kariotypes/ext6.png'));
originalImageSize = size(kariotypeImg);
originalAspectRatio = originalImageSize(2)/originalImageSize(1);


display('resizing image')
gaussianKernel = fspecial('gaussian', round(originalImageSize(1)/10), round(originalImageSize(1)/20));
filteredImage = imfilter(kariotypeImg, gaussianKernel);
normalizedHeight = 1000;

resizedImage = imresize(kariotypeImg, normalizedHeight/originalImageSize(1));
resizedImageSize = size(resizedImage);



display('segmenting image by edges')
edgeBasedSegmentedImg = segmentByEdges(resizedImage);
display('segmenting image by std div')
stdDivBasedSegmentation = segmentByStdDiv(resizedImage);
display('segmenting image by histogram')
histEqBasedSegmentation = segmentByHistEqualization(resizedImage);
display('segmenting image by watershed')
watershedBasedSegmentation = segmentByWatershed(resizedImage);
figure; imshow(~(im2bw(resizedImage, graythresh(resizedImage))));

subplotX = 3;
subplotY = 2;

figure
subplot(subplotY, subplotX, 1)
imshow(resizedImage,[]);
title('Input Image');

subplot(subplotY, subplotX, 2)
imshow(edgeBasedSegmentedImg,[]);
title('Edge based segmentation Image');

subplot(subplotY, subplotX, 3)
imshow(histEqBasedSegmentation,[]);
title('Histogram based segmentation Image');

subplot(subplotY, subplotX, 4)
imshow(watershedBasedSegmentation,[]);
title('Watershed based segmentation Image');

subplot(subplotY, subplotX, 5)
imshow(stdDivBasedSegmentation,[]);
title('Std div based segmentation Image');


display('fusing segmented images')
orSegmentedImage = edgeBasedSegmentedImg | stdDivBasedSegmentation | histEqBasedSegmentation;
andSegmentedImage = edgeBasedSegmentedImg & stdDivBasedSegmentation & histEqBasedSegmentation;
votingImage = uint8(edgeBasedSegmentedImg) + uint8(histEqBasedSegmentation) + uint8(stdDivBasedSegmentation);
votingImage(votingImage < 2) = 0;
votingImage = logical(votingImage);



segmentedImage = uint8(zeros(size(resizedImage)));
segmentedIndexes = find(votingImage == 1);
segmentedImage(segmentedIndexes) = resizedImage(segmentedIndexes);


figure
imshow(segmentedImage);
imwrite(segmentedImage,'segmentedKariotype.bmp');

figure
imshow(votingImage,[]);
imwrite(votingImage,'segmentationMask.bmp');
