function [ pairedIndexes, pairtedImages  ] = pairChromosomesByAxisCorrelation( chromosomes, skeletons )
%PAIRCHROMOSOMESBYAXISCORRELATION Summary of this function goes here
%   Detailed explanation goes here

    %% Create every chromosome axis crop
    crops = uint8([]);
    for i = 1 : size(chromosomes,3)
        axisIndexes = find(skeletons(:,:,i)' == 1);       
        chrom = chromosomes(:,:,i)';      
        crop = chrom(axisIndexes);
                
        yMax = max(size(crops,1), size(crop,1));
        
        crop(yMax) = 0;
        crops(yMax,i) = 0;
        crops(:,i) = crop;
    end

    
    %% Create covariace matrix of cross correlations
    
    C = double(zeros(size(chromosomes,3)));
    for i = 1 : size(chromosomes,3)
        for j = 1 : size(chromosomes,3)
            if (i == j )
                C(i,j) = 0;
            else
                C(i,j) = max(normxcorr2(crops(i,:), crops(j,:)));
            end
        end
    end
    
    
    % pair images by C matrix
    pairedIndexes = zeros(size(chromosomes,3)/2,2);

    iterator = 1;
    for i = 1 : size(chromosomes,3)
        if max(C(i,:)) == 0
            continue
        end

        pairedIndexes( iterator, 1) = i;
        nearestIndex = find( C(i,:) == max(C(i,:)));
        pairedIndexes( iterator, 2) = nearestIndex;
        
        C(i,:) = 0;
        C(:,i) = 0;
        C(nearestIndex,:) = 0;
        C(:,nearestIndex) = 0;
        iterator = iterator+1;
    end
    
    
    %create new paired images 
    maxX = size(chromosomes,2);
    maxY = size(chromosomes,1);
    pairtedImages = uint8(zeros(maxY, 2*maxX, size(chromosomes,3)/2));
    for i = 1 : size(chromosomes,3)/2
        index1 = pairedIndexes(i,1);
        index2 = pairedIndexes(i,2);
        pairtedImages(:,:,i) = [chromosomes(:,:,index1)  chromosomes(:,:,index2)];
    end
    
end

